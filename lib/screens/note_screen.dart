import 'package:flutter/material.dart';

class NoteScreen extends StatefulWidget {
  // membuat model
  final String initialTitle;
  final String initialDeskripsi;
  final Color initialColor;

  const NoteScreen({
    Key? key,
    this.initialTitle = '',
    this.initialDeskripsi = '',
    this.initialColor = Colors.white,
  }) : super(key: key);

  @override
  State<NoteScreen> createState() => _NoteScreenState();
}

class _NoteScreenState extends State<NoteScreen> {
  late TextEditingController _titleNote;
  late TextEditingController _deskripsiNote;
  late Color selectedColor;

  @override
  void initState() {
    super.initState();
    // menetapkan nilai awal
    _titleNote = TextEditingController(text: widget.initialTitle);
    _deskripsiNote = TextEditingController(text: widget.initialDeskripsi);
    selectedColor = widget.initialColor;
  }

  // membuat fungsi dispose
  @override
  void dispose() {
    _titleNote.dispose();
    _deskripsiNote.dispose();
    super.dispose();
  }

  // membuat fungsi untuk menyimpan note
  void _saveNote() {
    final title = _titleNote.text;
    final deskripsi = _deskripsiNote.text;
    final color = selectedColor.value.toString();

    // Pengecekan apakah title dan deskripsi sudah diisi
    if (title.isEmpty && deskripsi.isEmpty) {
      const snackBar = SnackBar(
        content: Text('Title or description must be filled'),
        duration: Duration(seconds: 2),
      );

      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return; // Keluar dari fungsi jika title atau deskripsi kosong
    }

    // final color = selectedColor != Colors.white ? selectedColor.value.toString() : Colors.grey.value.toString();
    // print(title);
    // print(deskripsi);
    // print(color);

    Map<String, String> noteData = {
      'title': title,
      'deskripsi': deskripsi,
      'color': color,
    };

    Navigator.pop(context, noteData);

    // Menampilkan Snackbar sesuai kondisi mode
    final snackBar = SnackBar(
      content: Text(widget.initialTitle.isNotEmpty
          ? 'Note edited successfully'
          : 'Note created successfully'),
      duration: const Duration(seconds: 2),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  // membuat list warna
  List<Color> color = [
    Colors.orange,
    Colors.green,
    Colors.yellow,
    Colors.white,
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: selectedColor,
      appBar: AppBar(
        title: Text(widget.initialTitle.isNotEmpty
            ? 'Edit Note'
            : 'Create Note'), // pemberian kondisi untuk mode create atau edit
        backgroundColor: Colors.green,
      ),
      body: Column(
        children: [
          // textfield ke 1
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: TextField(
              controller: _titleNote,
              decoration: const InputDecoration(
                  border: InputBorder.none, hintText: 'Title'),
            ),
          ),
          // textfield ke 2
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
            child: TextField(
              controller: _deskripsiNote,
              maxLines: null,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              decoration: const InputDecoration(
                  border: InputBorder.none,
                  hintText: 'Write something here...',
                  hintStyle: TextStyle(fontSize: 13)),
            ),
          ),
        ],
      ),

      // membuat 2 floating button menggunakan stack dan positioned
      floatingActionButton: Stack(
        children: [
          // floating button ke 1
          Positioned(
            bottom: 16.0,
            right: 16.0,
            child: GestureDetector(
              onTap: _saveNote,
              child: Container(
                width: 56.0,
                height: 56.0,
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 4,
                      offset: const Offset(0, 2),
                    ),
                  ],
                ),
                child: const Icon(Icons.save, color: Colors.black),
              ),
            ),
          ),
          // floating button ke 2
          Positioned(
            bottom: 80.0,
            right: 23.0,
            child: GestureDetector(
              onTap: () {
                modalBottomSheetColor(context);
              },
              child: Container(
                width: 46.0,
                height: 46.0,
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      spreadRadius: 2,
                      blurRadius: 4,
                      offset: const Offset(0, 2),
                    ),
                  ],
                ),
                child: const Icon(Icons.palette),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // fungsi untuk menampilkan bottomsheet, saya pisah dengan diekstrak agar lebih rapih
  Future<dynamic> modalBottomSheetColor(BuildContext context) {
    return showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      builder: (context) {
        return SizedBox(
          height: 300,
          child: Column(
            children: [
              // Memakai stack untuk membuat garis strip di atas
              Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Divider(),
                        Container(
                          width: 50,
                          height: 5,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey,
                          ),
                        ),
                        const Divider(),
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: color.length,
                  itemBuilder: (context, index) {
                    Color currentColor = color[index];
                    String colorName = _getColorName(currentColor);

                    return ListTile(
                      onTap: () {
                        setState(() {
                          selectedColor = currentColor;
                        });
                        Navigator.pop(context);
                      },
                      leading: CircleAvatar(
                        backgroundColor: currentColor,
                        radius: 20,
                      ),
                      title: Text(
                        colorName,
                        style: const TextStyle(color: Colors.black),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  String _getColorName(Color color) {
    if (color == Colors.orange) {
      return "Orange";
    } else if (color == Colors.green) {
      return "Green";
    } else if (color == Colors.yellow) {
      return "Yellow";
    } else if (color == Colors.white) {
      return "None";
    } else {
      return "Unknown"; // Default jika warna tidak cocok
    }
  }
}
