import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tugas3_aplikasi_note/screens/note_screen.dart';

import 'detail_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<Map<String, dynamic>> _notes = []; //membuat list kosong
  bool _isGridView =
      false; // membuat bool _isGridiView dan set defaultnya false
  bool _isBottomSheetVisible =
      false; // membuat bool _isBottomSheetVisible dan set defaultnya false

  // membuat fungsi untuk merubah status visibility bottom sheet
  void _toggleBottomSheet(bool isVisible) {
    setState(() {
      _isBottomSheetVisible = isVisible;
    });
  }

  // membuat fungsi untuk navigasi ke note screen
  Future<void> _navigateToNoteScreen({Map<String, dynamic>? noteData}) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => NoteScreen(
          initialTitle: noteData?['title'] ?? '', // mengambil data title
          initialDeskripsi:
              noteData?['deskripsi'] ?? '', // mengambil data deskripsi
          initialColor: noteData != null // mengambil data color
              ? Color(int.parse(noteData['color'] as String))
              : Colors.white,
        ),
      ),
    );

    // Cek apakah hasil dari layar NoteScreen tidak null dan berupa Map<String, dynamic>
    if (result != null && result is Map<String, dynamic>) {
      if (noteData != null) {
        final editIndex =
            _notes.indexOf(noteData); // mencari index yang diedit dalam _notes
        if (editIndex >= 0) {
          // jika index ditemukan, artinya catatan sedang diedit
          setState(() {
            _notes[editIndex] = result; // mengganti data catatan
          });
        }
      } else {
        // jika data null
        setState(() {
          _notes.add(result); //menambahkan catatan baru
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text('Notes'),
            IconButton(
              onPressed: () {
                // pemberian kondisi pada bool _isGridView
                setState(() {
                  _isGridView = !_isGridView;
                });
              },
              icon: _isGridView
                  ? const Icon(Icons.list) // jika _isGridView false
                  : const Icon(Icons.grid_view), // jika _isGridView true
            ),
          ],
        ),
        backgroundColor: Colors.green,
      ),
      body: Stack(
        children: [
          if (_isBottomSheetVisible) // membuat efek blur saat bottom sheet muncul
            BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
              child: Container(color: Colors.transparent),
            ),
          // pengecekan, jika _note kosong maka akan menampilkan teks
          if (_notes.isEmpty)
            const Center(
              child: Text('Make your first note'),
            ),
          _isGridView // pemberian kondisi jika icon list atau grid ditekan pada appbar
              ? GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  itemCount: _notes.length,
                  itemBuilder: (context, index) {
                    // Color noteColor =
                    //     Color(int.parse(_notes[index]['color'] as String));

                    // mengambil color dari _notes
                    Color noteColor = _notes[index]['color'] != null
                        ? Color(int.parse(_notes[index]['color'] as String))
                        : Colors.grey;

                    // mengubah color menjadi grey jika white, untuk warna pada container list maupun grid
                    Color containerColor = noteColor;
                    if (noteColor == Colors.white) {
                      containerColor = Colors.grey;
                    }

                    return GestureDetector(
                      onLongPress: () {
                        _toggleBottomSheet(true); // akan bernilai true
                        bottomSheetLongPress(
                            context, index); // memanggil methode
                      },
                      onTap: () {
                        // navigasi ke halaman detail screen dengan membawa parameter title, deskripsi, dan noteColor
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailScreen(
                              title: _notes[index]['title'] ?? '',
                              deskripsi: _notes[index]['deskripsi'] ?? '',
                              noteColor: noteColor,
                            ),
                          ),
                        );
                      },
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        width: double.infinity,
                        height: double.infinity,
                        child: Card(
                          elevation: 2,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          color: containerColor,
                          child: Padding(
                            padding: const EdgeInsets.all(20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 5),
                                  child: Text(
                                    _notes[index]['title'] ?? '',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                Text(
                                  _notes[index]['deskripsi'] ?? '',
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                )
              : ListView.builder(
                  itemCount: _notes.length,
                  itemBuilder: (context, index) {
                    // Color noteColor =
                    //     Color(int.parse(_notes[index]['color'] as String));

                    // mengambil data color
                    Color noteColor = _notes[index]['color'] != null
                        ? Color(int.parse(_notes[index]['color'] as String))
                        : Colors.grey;

                    // mengubah warna dari white ke grey untuk tampilan list maupun grid
                    Color containerColor = noteColor;
                    if (noteColor == Colors.white) {
                      containerColor = Colors.grey;
                    }

                    return GestureDetector(
                      onLongPress: () {
                        _toggleBottomSheet(true);
                        showModalBottomSheet(
                          context: context,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                          ),
                          builder: (context) {
                            return SizedBox(
                              height: 170,
                              child: Column(
                                children: [
                                  Stack(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Divider(),
                                            Container(
                                              width: 50,
                                              height: 5,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                color: Colors.grey,
                                              ),
                                            ),
                                            const Divider(),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 50, 20, 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        const Text('Actions'),
                                        const Spacer(),
                                        Container(
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.green,
                                          ),
                                          child: IconButton(
                                            onPressed: () {
                                              setState(() {
                                                _notes.removeAt(index);
                                                _toggleBottomSheet(false);
                                              });
                                              Navigator.pop(context);
                                            },
                                            icon: const Icon(Icons.delete,
                                                color: Colors.white),
                                          ),
                                        ),
                                        const SizedBox(width: 15),
                                        Container(
                                          decoration: const BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.green,
                                          ),
                                          child: IconButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                              _toggleBottomSheet(false);
                                              _navigateToNoteScreen(
                                                  noteData: _notes[index]);
                                            },
                                            icon: const Icon(Icons.create,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailScreen(
                              title: _notes[index]['title'] ?? '',
                              deskripsi: _notes[index]['deskripsi'] ?? '',
                              noteColor: noteColor,
                            ),
                          ),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(20, 15, 20, 0),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: containerColor,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 10, 10, 3),
                                child: Text(_notes[index]['title'] ?? ''),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 0, 10, 10),
                                child: Text(
                                  _notes[index]['deskripsi'] ?? '',
                                  maxLines: null,
                                  overflow: TextOverflow.clip,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
        ],
      ),

      // floating action button, yaitu tombol yang melayang di sebelah kanan bawah
      floatingActionButton: GestureDetector(
        onTap: _navigateToNoteScreen,
        child: Container(
          width: 56.0,
          height: 56.0,
          decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.circular(15.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                spreadRadius: 2,
                blurRadius: 4,
                offset: const Offset(0, 2),
              ),
            ],
          ),
          child: const Icon(Icons.add, color: Colors.white),
        ),
      ),
    );
  }

  // ini adalah showmodalbottomsheet yang diekstrak agar lebih rapih
  Future<dynamic> bottomSheetLongPress(BuildContext context, int index) {
    return showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      builder: (context) {
        return SizedBox(
          height: 170,
          child: Column(
            children: [
              Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Divider(),
                        Container(
                          width: 50,
                          height: 5,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.grey,
                          ),
                        ),
                        const Divider(),
                      ],
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(20, 50, 20, 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text('Actions'),
                    const Spacer(),
                    Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.green,
                      ),
                      child: IconButton(
                        onPressed: () {
                          setState(() {
                            _notes.removeAt(index);
                            _toggleBottomSheet(false);
                          });

                          // Menampilkan Snackbar sebelum keluar dari DetailScreen
                          const snackBar = SnackBar(
                            content: Text('Note deleted successfully!'),
                            duration: Duration(seconds: 2),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);

                          Navigator.pop(context); // Keluar dari DetailScreen
                        },
                        icon: const Icon(Icons.delete, color: Colors.white),
                      ),
                    ),
                    const SizedBox(width: 15),
                    Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.green,
                      ),
                      child: IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                          _toggleBottomSheet(false);
                          _navigateToNoteScreen(noteData: _notes[index]);
                        },
                        icon: const Icon(Icons.create, color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
