import 'package:flutter/material.dart';

class DetailScreen extends StatelessWidget {
  final String title;
  final String deskripsi;
  final Color noteColor;
  const DetailScreen(
      {super.key,
      required this.title,
      required this.deskripsi,
      required this.noteColor});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: noteColor,
      appBar: AppBar(
        title: const Text('Note Detail'),
        backgroundColor: Colors.green,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
            ),
            const SizedBox(height: 10),
            Text(
              deskripsi,
            ),
          ],
        ),
      ),
    );
  }
}
